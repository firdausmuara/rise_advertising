<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Rise Advertising</title>
	<link rel="icon" href="{{ asset('po-content/frontend/fimply/img/favicon.ico') }}">

	<!--Google web fonts-->
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,300italic,400italic,600,700,600italic,200,200italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'>

	<!-- Bootstrap -->
	<link href="{{ asset('po-content/frontend/fimply/css/bootstrap.min.css')}}" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="{{ asset('po-content/frontend/fimply/css/animate.css')}}">

	<!-- font awesome-->
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('po-content/frontend/fimply/css/skillset.css')}}">


	<link rel="stylesheet" type="text/css" href="{{ asset('po-content/frontend/fimply/css/owl.carousel.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('po-content/frontend/fimply/css/owl.transitions.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('po-content/frontend/fimply/css/owl.theme.css')}}">

	<link rel="stylesheet" type="text/css" href="{{ asset('po-content/frontend/fimply/style.css')}}">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<!--preloader-->
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>

	<header class="main_header">
		<div class="row">
			<div class="content"> <a class="logo" href="#"><img src="{{ asset('po-content/frontend/fimply/img/logo.png') }}" alt="" width="150px" height="30px"></a>
				<div class="mobile-toggle"> <span></span> <span></span> <span></span></div>
				<nav>
					<ul class="main_menu">
						<li><a href=".hero">Beranda</a></li>
						<li><a href=".service_area">Layanan</a></li>
						<li><a href=".portfolio_area">Galeri</a></li>
						<li><a href=".blog_area">Blog</a></li>
						<li><a href=".contact_area">Kontak</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END row -->
	</header>

	@yield('content')

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="{{ asset('po-content/frontend/fimply/js/bootstrap.min.js')}}"></script>

	<script src="{{ asset('po-content/frontend/fimply/js/wow.js')}}"></script>

	<script src="{{ asset('po-content/frontend/fimply/js/jquery.nicescroll.js')}}"></script>

	<script type="text/javascript" src="{{ asset('po-content/frontend/fimply/js/jquery.easing.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('po-content/frontend/fimply/js/jquery.mixitup.min.js')}}"></script>
	<script src="{{ asset('po-content/frontend/fimply/js/imagesloaded.pkgd.min.js')}}"></script>

	<script src="{{ asset('po-content/frontend/fimply/js/skillset.js')}}"></script>

	<script src="{{ asset('po-content/frontend/fimply/js/owl.carousel.js')}}"></script>


	<script src="{{ asset('po-content/frontend/fimply/js/scrollupto.js')}}"></script>
	<script type="text/javascript" src="{{ asset('po-content/frontend/fimply/js/main.js')}}"></script>
</body>

</html>