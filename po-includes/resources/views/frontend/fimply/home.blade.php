@extends(getTheme('layouts.app'))

@section('content')

<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="2000" id="bs-carousel">
	<!-- Overlay -->
	<div class="overlay"></div>

	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
		<li data-target="#bs-carousel" data-slide-to="1"></li>
		<li data-target="#bs-carousel" data-slide-to="2"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item slides active">
			<div class="slide-1"></div>
			<div class="hero">
				<hgroup>
					<h1>Invitation Card <span>Specialist</span></h1>
					<h3>Customize Your Wedding Needs!</h3>
				</hgroup>
				<a href="https://api.whatsapp.com/send?phone=628119949311" class="btn btn-hero btn-lg" role="button">Order</a>
			</div>
		</div>
		<div class="item slides">
			<div class="slide-2"></div>
			<div class="hero">
				<hgroup>
					<h1>Invitation Card <span>Specialist</span></h1>
					<h3>Customize Your Wedding Needs!</h3>
				</hgroup>
				<a href="https://api.whatsapp.com/send?phone=628119949311" class="btn btn-hero btn-lg" role="button">Order</a>
			</div>
		</div>
		<div class="item slides">
			<div class="slide-3"></div>
			<div class="hero">
				<hgroup>
					<h1>Invitation Card <span>Specialist</span></h1>
					<h3>Customize Your Wedding Needs!</h3>
				</hgroup>
				<a href="https://api.whatsapp.com/send?phone=628119949311" class="btn btn-hero btn-lg" role="button">Order</a>
			</div>
		</div>
	</div>
</div>

<div class="service_area">
	<div class="container">
		<div class="row">
			<div class="service_section wow bounceInLeft animated">
				<div class="col-md-3">
					<div class="single_service">
						<div class="ico"><i class="fa fa-mobile"></i></div>
						<h2>Pelayanan Terbaik</h2>
						<p>Pelayanan yang dilakukan secara ramah tamah dan dengan etika yang baik sehingga memenuhi kebutuhan dan kepuasan bagi yang menerima</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="single_service">
						<div class="ico"><i class="fa fa-code"></i></div>
						<h2>Kualitas Terbaik</h2>
						<p>Tingkat mutu yang diharapkan dan pengendalian keragaman dalam mencapai mutu tersebut untuk memenuhi kebutuhan konsumen</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="single_service">
						<div class="ico"><i class="fa fa-paint-brush"></i></div>
						<h2>Desain Modern</h2>
						<p>Desain yang simple, bersih, fungsional,stylish dan selalu mengikuti perkembangan jaman yang berkaitan dengan gaya hidup modern yang sedang berkembang pesat</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="single_service">
						<div class="ico"><i class="fa fa-money"></i></div>
						<h2>Harga Terjangkau</h2>
						<p>Harga yang terjangkau adalah harapan konsumen sebelum mereka melakukan pembelian</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div><!-- service area end   -->

<div class="portfolio_area">
	<div class="container">
		<div class="portfolio_section">
			<div class="col-md-12 portfolio_top">
				<h1>Galeri</h1>
				<p>MURAH <b>YES</b>, MURAHAN <b>NO</b></p>
			</div>
		</div>

		<ul id="filters" class="clearfix">
			<li><span class="filter active" data-filter="app card icon logos web">Semua Kategori</span></li>
			<li><span class="filter" data-filter="app">Kategori 1</span></li>
			<li><span class="filter" data-filter="card">Kategori 2</span></li>
		</ul>

		<div id="portfoliolist">

			<div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/1.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/2.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/3.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/4.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/5.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/6.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/7.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/8.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/9.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/10.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/11.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/12.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/13.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">
					<img src="{{ asset('po-content/frontend/fimply/img/portfolios/14.jpg')}}" alt="" />
					<div class="label">
						<div class="label-text">
							<span class="text-category">INVITATION CARD</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

		</div>

	</div>
</div><!-- portfolio area end   -->

<!-- <div class="blog_area">
	<div class="container">
		<div class="row">
			<div class="blog_section">
				<h1><span>Blog</span> Terbaru</h1>
				<div class="single_blog col-md-4 slider-content">
					<a href="#">
						<img src="{{ asset('po-content/frontend/fimply/img/1.jpg')}}" alt="">
						<div class="slider-text">
							<h3>This is demo title</h3>
							<p> <i class="fa fa-user"></i> By FAHEM <i class="fa fa-clock-o"></i> August 31th, 2014</p>
							<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially.</p>
						</div>
					</a>
				</div>
				<div class="single_blog col-md-4 slider-content">
					<a href="#">
						<img src="{{ asset('po-content/frontend/fimply/img/2.jpg')}}" alt="">
						<div class="slider-text">
							<h3>This is demo title</h3>
							<p> <i class="fa fa-user"></i> By FAHEM <i class="fa fa-clock-o"></i> August 31th, 2014</p>
							<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially.</p>
						</div>
					</a>
				</div>
				<div class="single_blog col-md-4 slider-content">
					<a href="#">
						<img src="{{ asset('po-content/frontend/fimply/img/3.jpg')}}" alt="">
						<div class="slider-text">
							<h3>This is demo title</h3>
							<p> <i class="fa fa-user"></i> By FAHEM <i class="fa fa-clock-o"></i> August 31th, 2014</p>
							<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially.</p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div> -->
<!-- blog area end   -->


<!-- <div class="price_area">
	<div class="container">
		<div class="row">
			<div class="price_section">
				<h1>Paket</h1>
				<div class="col-md-12">
					<div class="pricing-table">
						<div class="col-md-4 col-sm-4 col-xs-12 pricing-box first wow bounceInUp animated">
							<ul>
								<li class="plan-title">
									Silver
								</li>
								<li class="subscription-price oswald">
									<span class="currency oswald">$</span>
									<span class="price">59</span>
									/ bulan
								</li>
								<li class="ptop10">
									5 Free Websites
								</li>
								<li>
									100 Gb Storage
								</li>
								<li>
									24 / 7 Full Support
								</li>
								<li>
									Cloud Backup
								</li>
								<li>
									5 Admin Panels
								</li>
								<li>
									Free Trials
								</li>
								<li class="last border-none pbottom40">
									No Bonus Points
								</li>
								<li class="sing-up">
									<a href="#" class="animate">Daftar</a>
								</li>
							</ul>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 pricing-box second featured wow bounceInUp animated">
							<ul>
								<li class="plan-title">
									Gold
								</li>
								<li class="subscription-price oswald">
									<span class="currency oswald">$</span>
									<span class="price">79</span>
									/ bulan
								</li>
								<li class="ptop10">
									20 Free Websites
								</li>
								<li>
									500 Gb Storage
								</li>
								<li>
									Full Support
								</li>
								<li>
									Cloud Backup
								</li>
								<li>
									20 Admin Panels
								</li>
								<li>
									Free Trials
								</li>
								<li class="last border-none pbottom40">
									All Bonus Points
								</li>
								<li class="sing-up">
									<a href="#" class="animate">Daftar</a>
								</li>
							</ul>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 pricing-box last wow bounceInUp animated">
							<ul>
								<li class="plan-title">
									Diamond
								</li>
								<li class="subscription-price oswald">
									<span class="currency oswald">$</span>
									<span class="price">99</span>
									/ bulan
								</li>
								<li class="ptop10">
									50 Free Websites
								</li>
								<li>
									1 Tb Storage
								</li>
								<li>
									Full Support
								</li>
								<li>
									Cloud Backup
								</li>
								<li>
									50 Admin Panels
								</li>
								<li>
									Free Trials
								</li>
								<li class="last border-none pbottom40">
									All Bonus Points
								</li>
								<li class="sing-up">
									<a href="#" class="animate">Daftar</a>
								</li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div> -->
<!-- price area end   -->


<!-- <div class="testimonial_area">
	<div class="container">
		<div class="row">
			<div class="testimonial_section">
				<h1>What our <span>client say</span></h1>
				<div id="testimonial_carosule" class="owl-carousel owl-theme">
					<div class="item">
						<blockquote>
							<img src="{{ asset('po-content/frontend/fimply/img/tm1.png')}}">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							</p>
						</blockquote>
						<h3>---- kopa Samsu || Kopa Master</h3>

					</div>
					<div class="item">
						<blockquote>
							<img src="{{ asset('po-content/frontend/fimply/img/tm1.png')}}">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							</p>
						</blockquote>
						<h3>---- Kuddus boyati || Baula</h3>

					</div>
					<div class="item">
						<blockquote>
							<img src="{{ asset('po-content/frontend/fimply/img/tm1.png')}}">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							</p>
						</blockquote>
						<h3>---- Rohima || Kamner Beti</h3>

					</div>
					<div class="item">
						<blockquote>
							<img src="{{ asset('po-content/frontend/fimply/img/tm1.png')}}">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							</p>
						</blockquote>
						<h3>---- Khairun sundori || Laika</h3>

					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->
<!-- testimonial area end   -->

<div class="contact_area">
	<div class="container">
		<div class="row">
			<div class="contact_section">
				<h1>Kontak Kami</h1>
				<p>Invitation Card Specialist | Customize Your Wedding Needs!</p>

				<div class="col-md-6">
					<div class="contact_form">
						@if (Session::has('flash_message'))
							<div class="alert alert-success">{{ Session::get('flash_message') }}</div>
						@endif
						
						@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						<form action="{{ url('contact/send') }}" method="post" role="form" class="php-email-form">

							@csrf
							@method('post')

							<fieldset id="contact_form">
								<div id="result"></div>
								<label for="name" class="form-group">
									<input type="text" name="name" id="name" placeholder="Enter Your Name" value="{{ old('name') }}" data-rule="minlen:4" data-msg="Please enter at least 4 chars"/>
									<div class="validate"></div>
								</label>

								<label for="email" class="form-group">
									<input type="email" name="email" id="email" placeholder="Enter Your Email" value="{{ old('email') }}" data-rule="email" data-msg="Please enter a valid email"/>
									<div class="validate"></div>
								</label>

								<label for="subject" class="form-group">
									<input type="text" name="subject" id="subject" placeholder="Enter Your Subject" value="{{ old('subject') }}" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
									<div class="validate"></div>
								</label>

								<label for="message" class="form-group">
									<textarea name="message" id="message" placeholder="Enter Your Message" data-rule="required" data-msg="Please write something for us">{{ old('message') }}</textarea>
									<div class="validate"></div>
								</label>
								<div class="mb-3">
									<div class="loading">Loading</div>
									<div class="error-message"></div>
									<div class="sent-message">Your message has been sent. Thank you!</div>
								</div>
								<button type="submit" class="submit_btn" id="submit_btn">Submit</button>
								</label>
							</fieldset>

						</form>
					</div>
				</div>
				<div class="col-md-6">
					<div class="contact_text">
						<p>Invitation Card Specialist | Customize Your Wedding Needs!</p>
						<h3>info kontak</h3>
						<ul class="contact_info">
							<li>riseadvertising.print@gmail.com</li>
							<li>Jl. Revolusi No.78, Sukamaju, Cilodong, Kota Depok, Jawa Barat 16415</li>
							<li>08119949311 (WhatsApp), 021-83717617 (Office)</li>
						</ul>
						<h3>follow us</h3>
						<ul class="contact_social">
							<a href="https://www.instagram.com/riseadvertising/">
								<li><i class="ig fa fa-instagram"></i></li>
							</a>
							<a href="mailto:riseadvertising.print@gmail.com">
								<li><i class="gp fa fa-envelope-square"></i></li>
							</a>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- contact area end   -->

@endsection

@push('script')
<script type="text/javascript">
    !(function($) {
        "use strict";

        $("form.php-email-form").submit(function(e) {
            e.preventDefault();

            var f = $(this).find(".form-group"),
                ferror = false,
                emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;

            f.children("input").each(function() {
                // run all inputs

                var i = $(this); // current input
                var rule = i.attr("data-rule");

                if (rule !== undefined) {
                    var ierror = false; // error flag for current input
                    var pos = rule.indexOf(":", 0);
                    if (pos >= 0) {
                        var exp = rule.substr(pos + 1, rule.length);
                        rule = rule.substr(0, pos);
                    } else {
                        rule = rule.substr(pos + 1, rule.length);
                    }

                    switch (rule) {
                        case "required":
                            if (i.val() === "") {
                                ferror = ierror = true;
                            }
                            break;

                        case "minlen":
                            if (i.val().length < parseInt(exp)) {
                                ferror = ierror = true;
                            }
                            break;

                        case "email":
                            if (!emailExp.test(i.val())) {
                                ferror = ierror = true;
                            }
                            break;

                        case "checked":
                            if (!i.is(":checked")) {
                                ferror = ierror = true;
                            }
                            break;

                        case "regexp":
                            exp = new RegExp(exp);
                            if (!exp.test(i.val())) {
                                ferror = ierror = true;
                            }
                            break;
                    }
                    i.next(".validate")
                        .html(
                            ierror ?
                            i.attr("data-msg") !== undefined ?
                            i.attr("data-msg") :
                            "wrong Input" :
                            ""
                        )
                        .show("blind");
                }
            });
            f.children("textarea").each(function() {
                // run all inputs

                var i = $(this); // current input
                var rule = i.attr("data-rule");

                if (rule !== undefined) {
                    var ierror = false; // error flag for current input
                    var pos = rule.indexOf(":", 0);
                    if (pos >= 0) {
                        var exp = rule.substr(pos + 1, rule.length);
                        rule = rule.substr(0, pos);
                    } else {
                        rule = rule.substr(pos + 1, rule.length);
                    }

                    switch (rule) {
                        case "required":
                            if (i.val() === "") {
                                ferror = ierror = true;
                            }
                            break;

                        case "minlen":
                            if (i.val().length < parseInt(exp)) {
                                ferror = ierror = true;
                            }
                            break;
                    }
                    i.next(".validate")
                        .html(
                            ierror ?
                            i.attr("data-msg") != undefined ?
                            i.attr("data-msg") :
                            "wrong Input" :
                            ""
                        )
                        .show("blind");
                }
            });
            if (ferror) return false;

            var this_form = $(this);
            var action = $(this).attr("action");

            if (!action) {
                this_form.find(".loading").slideUp();
                this_form
                    .find(".error-message")
                    .slideDown()
                    .html("The form action property is not set!");
                return false;
            }

            this_form.find(".sent-message").slideUp();
            this_form.find(".error-message").slideUp();
            this_form.find(".loading").slideDown();

            if ($(this).data("recaptcha-site-key")) {
                var recaptcha_site_key = $(this).data("recaptcha-site-key");
                grecaptcha.ready(function() {
                    grecaptcha
                        .execute(recaptcha_site_key, {
                            action: "php_email_form_submit"
                        })
                        .then(function(token) {
                            php_email_form_submit(
                                this_form,
                                action,
                                this_form.serialize() + "&recaptcha-response=" + token
                            );
                        });
                });
            } else {
                php_email_form_submit(this_form, action);
            }

            return true;
        });

        function php_email_form_submit(this_form, action) {
            $.ajax({
                    type: "POST",
                    url: action,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'name': $('#name').val(),
                        'email': $('#email').val(),
                        'subject': $('#subject').val(),
                        'message': $('#message').val(),
                    },
                    timeout: 40000,
                })
                .done(function(msg) {
                    if (msg) {
                        this_form.find(".loading").slideUp();
                        this_form.find(".sent-message").slideDown();
                        this_form.find("input:not(input[type=submit]), textarea").val("");
                    } else {
                        this_form.find(".loading").slideUp();
                        if (!msg) {
                            msg =
                                "Form submission failed and no error message returned from: " +
                                action +
                                "<br>";
                        }
                        this_form.find(".error-message").slideDown().html(msg);
                    }
                })
                .fail(function(data) {
                    console.log(data);
                    var error_msg = "Form submission failed!<br>";
                    if (data.statusText || data.status) {
                        error_msg += "Status:";
                        if (data.statusText) {
                            error_msg += " " + data.statusText;
                        }
                        if (data.status) {
                            error_msg += " " + data.status;
                        }
                        error_msg += "<br>";
                    }
                    if (data.responseText) {
                        error_msg += data.responseText;
                    }
                    this_form.find(".loading").slideUp();
                    this_form.find(".error-message").slideDown().html(error_msg);

                });
        }
    })(jQuery);
</script>
@endpush